import React, { useRef, useEffect, useCallback } from "react";
import { Animated, Easing } from "react-native";
import PropTypes from "prop-types";

export default function FadeInImage({
  uri,
  style,
  blurRadius,
  onAnimationEnd
}) {
  const fade = useRef(new Animated.Value(0));

  useEffect(() => {
    if (uri) fadeIn();
  }, [fadeIn, uri]);

  const fadeIn = useCallback(() => {
    fade.current.setValue(0);
    Animated.timing(fade.current, {
      duration: 300,
      easing: Easing.sin,
      toValue: 1,
      useNativeDriver: true
    }).start(() => {
      if (typeof onAnimationEnd === "function") onAnimationEnd();
    });
  }, [onAnimationEnd]);
  if (!uri) return null;
  return (
    <Animated.Image
      style={[style, { opacity: fade.current }]}
      source={{ uri }}
      blurRadius={blurRadius}
    />
  );
}

FadeInImage.propTypes = {
  uri: PropTypes.string,
  style: PropTypes.any,
  blurRadius: PropTypes.number,
  onAnimationEnd: PropTypes.func
};
