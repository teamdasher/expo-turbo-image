import React, { useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet } from "react-native";
import * as FileSystem from "expo-file-system";
import md5 from "js-md5";
import FadeInImage from "./FadeInImage";
import useSafeState from "./useSafeState";

const CACHE_DIR = `${FileSystem.cacheDirectory}turbo-expo-image`;

async function uriToName(uri) {
  const hash = md5(uri);
  const formats = [".png", ".jpeg", ".bmp", ".gif", ".webp"];
  let ext = ".jpg";
  for (const format of formats) {
    if (uri.indexOf(format) > -1) {
      ext = format;
      break;
    }
  }

  try {
    await FileSystem.makeDirectoryAsync(CACHE_DIR);
  } catch (error) {
    // do nothing
  }
  return `${CACHE_DIR}/${hash}${ext}`;
}

async function loadImage({ uri, onStart, onLoaded, onError }) {
  try {
    uri = uri.replace(/ /gi, "+"); // THIS IS A TEMPORARY PATCH!!! TODO: ADDRESS B/E URL BUG
    const file = await uriToName(uri);
    const cached = await FileSystem.getInfoAsync(file);
    if (onStart) onStart();
    if (cached.exists && onLoaded) {
      onLoaded(cached.uri);
      return;
    }
    const res = await FileSystem.downloadAsync(uri, file);
    if (res && res.status === 200) {
      if (onLoaded) onLoaded(res.uri);
      return;
    }
    if (onError) onError(new Error(`Error downloading image - ${res.status}`));
  } catch (error) {
    if (onError) onError(error);
  }
}

function TurboImage({ uri, thumb, style, children }) {
  const [imageCachedURI, setImageCachedURI] = useSafeState();
  const [thumbCachedURI, setThumbCachedURI] = useSafeState();
  const [mainVisible, setMainVisible] = useSafeState(false);

  useEffect(() => {
    startLoading();
  }, [uri, startLoading]);

  const startLoading = useCallback(async () => {
    if (typeof uri !== "string") return;
    if (/^file:\/\//.test(uri)) {
      // local file - no need to cache
      setImageCachedURI(uri);
      return;
    }
    loadImage({
      uri: uri,
      onStart: () => {
        setImageCachedURI(null);
        setThumbCachedURI(null);
        setMainVisible(false);
        if (!thumb) return;
        loadImage({
          uri: thumb,
          onLoaded: setThumbCachedURI,
        });
      },
      onLoaded: setImageCachedURI,
    });
  }, [uri, thumb]);

  return (
    <View style={[style, styles.main]}>
      {!mainVisible && thumb && (
        <FadeInImage
          uri={thumbCachedURI}
          style={styles.absolute}
          blurRadius={15}
          key={thumbCachedURI + "tmb"}
        />
      )}
      <FadeInImage
        uri={imageCachedURI}
        style={styles.absolute}
        key={imageCachedURI + "img"}
        onAnimationEnd={() => setMainVisible(true)}
      />
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  main: { padding: 0, backgroundColor: "#f7f7f7" },
  absolute: { position: "absolute", top: 0, right: 0, bottom: 0, left: 0 },
});

export default React.memo(TurboImage);

TurboImage.propTypes = {
  uri: PropTypes.string,
  thumb: PropTypes.string,
  style: PropTypes.any,
  children: PropTypes.node,
};
