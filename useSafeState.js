import { useState, useRef, useEffect } from "react";

function useSafeState(initial) {
  const mounted = useRef(false);
  const [state, setState] = useState(initial);

  useEffect(() => {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  }, []);

  const safeSet = (state) => {
    if (!mounted.current) return;
    setState(state);
  };

  return [state, safeSet];
}

export default useSafeState;
